#include <stdio.h>
#include <stdlib.h>
#include<time.h>


int* genRandArray(int size, int maxValue);
void print(int* arr);

int main(){
    srand(time(NULL));
    int size = rand()%10;
    int maxValue = 100;
    int* arr = genRandArray(size, maxValue);
    print(arr);
    //очистка выделенной памяти
    free(arr);
}

int* genRandArray(int size, int maxValue) {
    int* array = (int*)malloc(size * sizeof(int));
    array[0] = size;
    for (int i = 1; i < size; i++) {
        array[i] = rand() % maxValue;
        //printf("%d ", array[i]);
    }
    return array;
}

void print(int* arr) {
    printf("%d: ", arr[0]);
    for (int i = 1; i < arr[0]; i++)
        printf("%d ", arr[i]);
}